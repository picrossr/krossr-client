import { Component } from '@angular/core';

/**
 * Just a wrapper for the loading animation
 * to prevent needing to be super repetitive in popup content templates
 */
@Component({
    selector: 'krossr-popup-loading-animation',
    templateUrl: './PopupLoadingAnimationView.html'
})
export class PopupLoadingAnimationComponent {
}
