import { NgModule } from '@angular/core';
import { KrossrButtonComponent } from './KrossrButtonComponent';

@NgModule({
    declarations: [
        KrossrButtonComponent
    ],
    entryComponents: [
        KrossrButtonComponent
    ],
    exports: [
        KrossrButtonComponent
    ]
})
export class KrossrButtonModule {
}
